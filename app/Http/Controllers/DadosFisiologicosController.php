<?php

namespace App\Http\Controllers;

use App\Models\DadosFisiologicos;
use Illuminate\Http\Request;
use App\Models\Prescricao;
use App\Messages\Message;
use App\Http\Requests;
use App\Models\Alerta;
use Response;


class DadosFisiologicosController extends Controller
{
    private $dados_fisiologicos;
    public function __construct(DadosFisiologicos $dadosFisiologicos)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        $this->dados_fisiologicos=$dadosFisiologicos;
    }

    public function allDadosFisiologicos(){
        return Response::json($this->dados_fisiologicos->allDadosfisiologicos(), 200);
    }

    public function getDadosFisiologicos($id){
        $dados = $this->dados_fisiologicos->getDadosFisiologicos($id);

        if(!$dados){
            return Response::json(['response'=>Message::getMsg('RF01')], 400);
        }
        return Response::json($dados, 200);
    }

    public function saveDadosFisiologicos(){
        $dados = $this->dados_fisiologicos->saveDadosFisiologicos();

        if(!$dados){
            return Response::json(['response'=>Message::getMsg('RF02')]);
        }
        return Response::json(['response'=>Message::getMsg('RS01')], 200);
    }

    public function updateDadosFisiologicos($id){
        $dados = $this->dados_fisiologicos->updateDadosFisiologicos($id);

        if(!$dados){
            return Response::json(['response'=>Message::getMsg('RF01')], 400);
        }
        return Response::json($dados, 200);
    }

    public function deleteDadosFisiologicos($id){
        if($this->dados_fisiologicos->deleteDadosFisiologicos($id)){
            return Response::json(['response'=>Message::getMsg('RS02')]);
        }
        return Response::json(['response'=>Message::getMsg('ER01')], 400);
    }
}

