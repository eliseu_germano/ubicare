<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DadosPressao;
use App\Messages\Message;
use App\Http\Requests;
use Response;

class DadosPressaoController extends Controller
{
    private $dadosPressao;

    public function __construct(DadosPressao $dadosPressao){
    	header('Access-Control-Allow-Origin: *'); 
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        $this->dadosPressao = $dadosPressao;
    }


    public function allDadosPressao(){
    	return Response::json($this->dadosPressao->allDadosPressao(), 200);
    }

	public function getDadosPressao($id){
		$dadosPressao = $this->dadosPressao->getDadosPressao($id);

		if(!$dadosPressao){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($dadosPressao, 200);
	}
	
	public function saveDadosPressao(){
		$dadosPressao = $this->dadosPressao->saveDadosPressao();
		
		if(!$dadosPressao){
			return Response::json(['response'=>Message::getMsg('RF02')]);
		}
		return Response::json(['response'=>Message::getMsg('RS01')], 200);
	}

	public function updateDadosPressao($id){
		$dadosPressao = $this->dadosPressao->updateDadosPressao($id);

		if(!$dadosPressao){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($dadosPressao, 200);
	}

	public function deleteDadosPressao($id){
		if($this->dadosPressao->deleteDadosPressao($id)){
			return Response::json(['response'=>Message::getMsg('RS02')]);
		}
		return Response::json(['response'=>Message::getMsg('ER01')], 400);
	}
}
