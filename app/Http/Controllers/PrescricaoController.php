<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Prescricao;
use App\Messages\Message;
use App\Http\Requests;
use App\Models\Alerta;
use Response;

class PrescricaoController extends Controller
{
	private $prescricao;

    public function __construct(Prescricao $prescricao){
    	header('Access-Control-Allow-Origin: *'); 
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        $this->prescricao = $prescricao;
    }


    public function allPrescricoes(){
    	return Response::json($this->prescricao->allPrescricoes(), 200);
    }

	public function getPrescricao($id){
		$prescricao = $this->prescricao->getPrescricao($id);

		if(!$prescricao){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($prescricao, 200);
	}
	
	public function savePrescricao(){
		$prescricao = $this->prescricao->savePrescricao();
		
		if(!$prescricao){
			return Response::json(['response'=>Message::getMsg('RF02')]);
		}
		return Response::json(['response'=>Message::getMsg('RS01')], 200);
	}

	public function updatePrescricao($id){
		$prescricao = $this->prescricao->updatePrescricao($id);

		if(!$prescricao){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($prescricao, 200);
	}

	public function deletePrescricao($id){
		if($this->prescricao->deletePrescricao($id)){
			return Response::json(['response'=>Message::getMsg('RS02')]);
		}
		return Response::json(['response'=>Message::getMsg('ER01')], 400);
	}

	public function getAlertas($id){
		return Prescricao::find($id)->alertas;
	}
}
