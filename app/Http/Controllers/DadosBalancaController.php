<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DadosBalanca;
use App\Messages\Message;
use App\Http\Requests;
use Response;

class DadosBalancaController extends Controller
{
    private $dadosBalanca;

    public function __construct(DadosBalanca $dadosBalanca){
    	header('Access-Control-Allow-Origin: *'); 
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        $this->dadosBalanca = $dadosBalanca;
    }


    public function allDadosBalanca(){
    	return Response::json($this->dadosBalanca->allDadosBalanca(), 200);
    }

	public function getDadosBalanca($id){
		$dadosBalanca = $this->dadosBalanca->getDadosBalanca($id);

		if(!$dadosBalanca){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($dadosBalanca, 200);
	}
	
	public function saveDadosBalanca(){
		$dadosBalanca = $this->dadosBalanca->saveDadosBalanca();
		
		if(!$dadosBalanca){
			return Response::json(['response'=>Message::getMsg('RF02')]);
		}
		return Response::json(['response'=>Message::getMsg('RS01')], 200);
	}

	public function updateDadosBalanca($id){
		$dadosBalanca = $this->dadosBalanca->updateDadosBalanca($id);

		if(!$dadosBalanca){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($dadosBalanca, 200);
	}

	public function deleteDadosBalanca($id){
		if($this->dadosBalanca->deleteDadosBalanca($id)){
			return Response::json(['response'=>Message::getMsg('RS02')]);
		}
		return Response::json(['response'=>Message::getMsg('ER01')], 400);
	}
}
