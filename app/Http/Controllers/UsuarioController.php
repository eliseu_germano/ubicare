<?php

namespace App\Http\Controllers;

use App\Library\ValidadorPerfil;
use App\Models\APIToken;
use App\Models\Usuario;
use App\Models\Perfil;
use App\Models\PerfilFactory;
use App\Models\Profissional;
use App\Messages\Message;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\Input;

class UsuarioController extends Controller
{
    private $usuario;
    private $perfil;

    public function __construct(Usuario $usuario){
    	header('Access-Control-Allow-Origin: *'); 
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        $this->usuario = $usuario;
    }

    public function ping() {
        return Response::json('OK', 200);
    }

    public function login() {
        $input = Input::all();

        $validation = Validator::make($input,
            [
                'login' => 'required|string|alpha_num',
                'senha' => 'required|string'
            ]);

        if($validation->fails())
            return Response::json(['response'=>Message::getMsg('SE04')], 400);

        $usuario = Usuario::where('login', '=', $input['login'])->first();

        if($usuario && Hash::check($input['senha'], $usuario->senha)) {
            $token = new APIToken;
            $token->token = bin2hex(openssl_random_pseudo_bytes(16));
            $token->chave_api = hash('sha256', $usuario->login.'&'.Carbon::now().'&'.openssl_random_pseudo_bytes(8).'=');
            $token->ip = Request::ip();
            $token->usuario = $usuario->id;
            $token->ultimo_uso = Carbon::now();
            $token->save();

            return Response::json([
                'response' => Message::getMsg('SE01'), 'chave_api' => $token->chave_api, 'token' => $token->token
            ], 200);
        }

        return Response::json(['response'=>Message::getMsg('SE02')], 401);
    }

    public function relogin() {
        $input = Input::all();

        $validation = Validator::make($input,
            [
                'token' => 'required|string|alpha_num',
                'assinatura' => 'required|string|alpha_num'
            ]);

        if($validation->fails())
            return Response::json(['response'=>Message::getMsg('SE04')], 400);

        $token = APIToken::where('token', '=', $input['token'])->first();
        if($token) {
            $token_assinatura = hash_hmac('sha256', $token->chave_api.'&'.$input['token'].'=', $token->chave_api);
            //dd($token_assinatura,
              //  $input["assinatura"]);
            if(hash_equals($token_assinatura, $input['assinatura'])) {
                $token->token = bin2hex(openssl_random_pseudo_bytes(16));
                $token->ip = Request::ip();
                $token->ultimo_uso = Carbon::now();
                $token->save();

                return Response::json([
                    'response' => Message::getMsg('SE01'), 'token' => $token->token
                ], 200);
            }
        }

        return Response::json(['response'=>Message::getMsg('SE02')], 401);
    }

    public function logout() {
        $input = Input::all();

        $validation = Validator::make($input,
            [
                'token' => 'required|string|alpha_num',
                'assinatura' => 'required|string|alpha_num'
            ]);

        if($validation->fails())
            return Response::json(['response'=>Message::getMsg('SE04')], 400);

        $token = APIToken::where('token', '=', $input['token'])->first();
        if($token) {
            $token_assinatura = hash_hmac('sha256', $token->chave_api.'&'.$input['token'].'=', $token->chave_api);

            if(hash_equals($token_assinatura, $input['assinatura'])) {
                $token->delete();

                return Response::json(['response' => Message::getMsg('SE01')], 200);
            }
        }

        return Response::json(['response' => Message::getMsg('SE02')], 401);
    }
    public function LoggedUsuario(){
        $token = Request::header('token');
        $id_usuario=APIToken::where('token','=',$token)->first();

        return $this->getUsuario($id_usuario->usuario);
    }
    public function allUsuarios(){
        $input = Input::all();

        $pagina = isset($input['pag']) ? $input['pag'] : 1;
        $qtd = isset($input['qtd']) ? $input['qtd'] : 20;
        $query_nome = isset($input['nome']) ? $input['nome'] : '';
        $query_email = isset($input['email']) ? $input['email'] : '';
        $query_perfil = isset($input['perfil']) ? $input['perfil'] : '';
        Paginator::currentPageResolver(function() use ($pagina) {
            return $pagina;
        });

        $dados = Usuario::where('nome', 'LIKE', '%'.$query_nome.'%')->
            where('perfil', 'LIKE','%'.$query_perfil.'%')->where('email', 'LIKE', '%'.$query_email.'%')->simplePaginate($qtd);

        $url_ant = null;
        if($pagina > 1) {
            $get = $input;
            $get['pag'] = $pagina - 1;

            $url_ant_get = implode('&', array_map(function($valor, $chave) {
                return $chave.'='.$valor;
            }, $get, array_keys($get)));

            $url_ant = url()->current().'?'.$url_ant_get;
        }
        $url_prox = null;
        if($dados->hasMorePages()) {
            $get = $input;
            $get['pag'] = $pagina + 1;

            $url_prox_get = implode('&', array_map(function($valor, $chave) {
                return $chave.'='.$valor;
            }, $get, array_keys($get)));

            $url_prox = url()->current().'?'.$url_prox_get;
        }

        foreach($dados->items() as $usuario) {
            $usuario['perfis'] = [$this->getPerfil($usuario['id'], $usuario['perfil'])];

            unset($usuario['created_at']);
    		unset($usuario['updated_at']);
    		unset($usuario['perfil']);
        }

        $result = [
            'dados' => $dados->items(),
            'paginacao' => [
                'pag' => $pagina,
                'qtd_por_pag' => $qtd,
                'url_ant' => $url_ant,
                'url_prox' => $url_prox
            ]
        ];

        return Response::json($result, 200);
    }

    public function getUsuario($id){
		$usuarioPerfil = array();
    	$usuarioPerfil['usuario'] = $this->usuario->getUsuario($id);
    	$usuarioPerfil['usuario']['perfis'] = [$this->getPerfil($id, $usuarioPerfil['usuario']['perfil'])];

    	unset($usuarioPerfil['usuario']['created_at']);
    	unset($usuarioPerfil['usuario']['updated_at']);
		return Response::json($usuarioPerfil, 200);
	}

	private function getPerfil($id, $tipo){
		$perfil = NULL;

		switch ($tipo) {
			case 'paciente':
				$perfil = Usuario::find($id)->paciente;
				break;
			
			case 'profissional':
				$perfil = Usuario::find($id)->profissional;
				break;

			case 'familia':
				$perfil = Usuario::find($id)->familia;
				break;

			default:
				return '[]';
		}
		if($perfil != NULL) $perfil['perfil'] = $tipo;

		unset($perfil['usuario_id']);
		unset($perfil['created_at']);
		unset($perfil['updated_at']);
		return $perfil;
	}

    public function saveUsuario($tipo){
		$id = $this->usuario->saveUsuario();
		$perfil = PerfilFactory::factoryMethod($tipo);
		$perfil = $perfil->savePerfil($id);

		if((!$id) && (!$perfilUsuario)){
			return Response::json(['response'=>Message::getMsg('RF02')], 200);
		}
		return Response::json(['response'=>Message::getMsg('RS01')], 200);
	}

	public function updateUsuario($id, $tipo){
		$usuario = $this->usuario->updateUsuario($id);
		$perfil = PerfilFactory::factoryMethod($tipo);
		$perfil = $perfil->updatePerfil($id);
		
		if((!$usuario) && (!$perfil)){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		unset($usuario['perfil']);
		return Response::json($usuario, 200);
	}

	public function deleteUsuario($idUsuario, $idPerfil, $tipo){
		$perfil = PerfilFactory::factoryMethod($tipo);
		$perfil = $perfil->deletePerfil($idPerfil);
		$usuario = $this->usuario->deleteUsuario($idUsuario);

		if((!$perfil) && (!$usuario)){
			return Response::json(['response'=>Message::getMsg('ER01')], 400);
		}
		return Response::json(['response'=>Message::getMsg('RS02')], 200);
	}

	
}
