<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alerta;
use App\Messages\Message;
use App\Http\Requests;
use Response;

class AlertaController extends Controller
{
    private $alerta;

    public function __construct(Alerta $alerta){
    	header('Access-Control-Allow-Origin: *'); 
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
        $this->alerta = $alerta;
    }
    
    public function allAlertas(){
    	return Response::json($this->alerta->allAlertas(), 200);
    }

	public function getAlerta($id){
		$alerta = $this->alerta->getAlerta($id);

		if(!$alerta){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($alerta, 200);
	}
	
	public function saveAlerta(){
		$alerta = $this->alerta->saveAlerta();
		
		if(!$alerta){
			return Response::json(['response'=>Message::getMsg('RF02')]);
		}
		return Response::json(['response'=>Message::getMsg('RS01')], 200);
	}

	public function updateAlerta($id){
		$alerta = $this->alerta->updateAlerta($id);

		if(!$alerta){
			return Response::json(['response'=>Message::getMsg('RF01')], 400);
		}
		return Response::json($alerta, 200);
	}

	public function deleteAlerta($id){
		if($this->alerta->deleteAlerta($id)){
			return Response::json(['response'=>Message::getMsg('RS02')]);
		}
		return Response::json(['response'=>Message::getMsg('ER01')], 400);
	}
}
