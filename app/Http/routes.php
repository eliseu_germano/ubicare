<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'seguranca'], function() {
    Route::post('login', ['uses' => 'UsuarioController@login']);
    Route::post('relogin', ['uses' => 'UsuarioController@relogin']);
    Route::post('logout', ['uses' => 'UsuarioController@logout']);
    Route::get('ping', ['uses' => 'UsuarioController@ping', 'middleware'=>'tokenauth']);
});


Route::group(['prefix'=>'gerencia', 'middleware'=>'tokenauth'], function(){

	/* Gerencia de Cuidados */

	Route::group(['prefix'=>'cuidados'], function(){

        Route::group(['prefix'=>'prescricao'], function(){
			Route::get('',['uses'=>'PrescricaoController@allPrescricoes']);
			Route::get('{id}',['uses'=>'PrescricaoController@getPrescricao']);
			Route::get('alertas/{id}',['uses'=>'PrescricaoController@getAlertas']);
			Route::post('',['uses'=>'PrescricaoController@savePrescricao', 'middleware'=>'requer_profissional']);
			Route::put('{id}',['uses'=>'PrescricaoController@updatePrescricao', 'middleware'=>'requer_profissional']);
			Route::delete('{id}',['uses'=>'PrescricaoController@deletePrescricao', 'middleware'=>'requer_profissional']);
		});

		Route::group(['prefix'=>'risco', 'middleware'=>'requer_profissional'], function(){
			Route::get('',['uses'=>'FatoresRiscoController@allFatores']);
			Route::get('{id}',['uses'=>'FatoresRiscoController@getFator']);
			Route::post('',['uses'=>'FatoresRiscoController@saveFator']);
			Route::put('{id}',['uses'=>'FatoresRiscoController@updateFator']);
			Route::delete('{id}',['uses'=>'FatoresRiscoController@deleteFator']);
		});

		Route::group(['prefix'=>'avaliacao', 'middleware'=>'requer_profissional'], function(){
			Route::get('',['uses'=>'AvaliacaoController@allAvaliacoes']);
			Route::get('{id}',['uses'=>'AvaliacaoController@getAvaliacao']);
			Route::post('',['uses'=>'AvaliacaoController@saveAvaliacao']);
			Route::put('{id}',['uses'=>'AvaliacaoController@updateAvaliacao']);
			Route::delete('{id}',['uses'=>'AvaliacaoController@deleteAvaliacao']);
		});

        Route::group(['prefix'=>'dadosfisiologicos'],function(){
            Route::get('',['uses'=>'DadosFisiologicosController@allDadosFisiologicos']);
            Route::get('{id}',['uses'=>'DadosFisiologicosController@getDadosFisiologicos']);
            Route::post('',['uses'=>'DadosFisiologicosController@saveDadosFisiologicos']);
            Route::put('{id}',['uses'=>'DadosFisiologicosController@updateDadosFisiologicos']);
            Route::delete('{id}',['uses'=>'DadosFisiologicosController@deleteDadosFisiologicos']);
        });

	});

	/* Gerencia de Notificacoes */

	Route::group(['prefix'=>'notificacao'], function(){
		Route::group(['prefix'=>'alertas'], function(){
			Route::get('',['uses'=>'AlertaController@allAlertas']);
			Route::get('{id}',['uses'=>'AlertaController@getAlerta']);
			Route::post('',['uses'=>'AlertaController@saveAlerta']);
			Route::put('{id}',['uses'=>'AlertaController@updateAlerta']);
			Route::delete('{id}',['uses'=>'AlertaController@deleteAlerta']);
		});
	});

	/* Gerencia de Usuarios */

	Route::group(['prefix'=>'usuario'], function(){

        Route::group(['prefix'=>'logged'],function (){
            Route::get('',['uses'=>'UsuarioController@LoggedUsuario']);
        });

        Route::get('',['uses'=>'UsuarioController@allUsuarios', 'middleware' => 'proibe_familia']);
		Route::get('{id}',['uses'=>'UsuarioController@getUsuario', 'middleware' => 'proibe_familia']);
        //falta colocar middleware
        Route::post('{perfil}',['uses'=>'UsuarioController@saveUsuario']);
		Route::put('{id}/{perfil}',['uses'=>'UsuarioController@updateUsuario']);
		Route::delete('{idUsuario}/{idPerfil}/{perfil}',['uses'=>'UsuarioController@deleteUsuario']);
	});

    //falta colocar middleware
    /* Gerencia de Dados de Sensores */

	Route::group(['prefix'=>'sensor'], function(){

		Route::group(['prefix'=>'balanca'], function(){
			Route::get('',['uses'=>'DadosBalancaController@allDadosBalanca']);
			Route::get('{id}',['uses'=>'DadosBalancaController@getDadosBalanca']);
			Route::post('',['uses'=>'DadosBalancaController@saveDadosBalanca']);
			Route::put('{id}',['uses'=>'DadosBalancaController@updateDadosBalanca']);
			Route::delete('{id}',['uses'=>'DadosBalancaController@deleteDadosBalanca']);
		});

		Route::group(['prefix'=>'pressao'], function(){
			Route::get('',['uses'=>'DadosPressaoController@allDadosPressao']);
			Route::get('{id}',['uses'=>'DadosPressaoController@getDadosPressao']);
			Route::post('',['uses'=>'DadosPressaoController@saveDadosPressao']);
			Route::put('{id}',['uses'=>'DadosPressaoController@updateDadosPressao']);
			Route::delete('{id}',['uses'=>'DadosPressaoController@deleteDadosPressao']);
		});

	});
	

	/* Gerencia de Relacionamentos */

	Route::group(['prefix'=>'Relacionamentos'], function(){
		return 'Relacionamentos';
	});
});

Route::get('/', function () {
    return view('welcome');
});
