<?php

namespace App\Http\Middleware;

use App\Messages\Message;
use App\Models\APIToken;
use Closure;

class ProibeProfissional
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario = APIToken::where('token', '=', $request->headers->get('token'))
            ->where('ip', '=', $request->ip())->first()->usuario_obj;

        if($usuario->perfil === 'profissional')
            return response(json_encode(['response' => Message::getMsg('SE05')]), 403);
        else
            return $next($request);
    }
}
