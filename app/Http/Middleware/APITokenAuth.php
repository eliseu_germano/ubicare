<?php

namespace App\Http\Middleware;

use App\Messages\Message;
use App\Models\APIToken;
use Carbon\Carbon;
use Closure;

class APITokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = APIToken::where('token', '=', $request->headers->get('token'))
            ->where('ip', '=', $request->ip())->first();

        if(!$token || Carbon::now()->diffInMinutes(new Carbon($token->ultimo_uso)) > 1)
            return response(json_encode(['response' => Message::getMsg('SE03')]), 401);
        else {
            $token->ultimo_uso = Carbon::now();
            $token->save();

            return $next($request);
        }
    }
}
