<?php 

namespace App\Messages;

final class Message {

	/**
	 * Mensagens usadas no sistema (Sucesso, Erros, Alertas)	
	 */

	private static $aMSG = array(
		'RS01' => 'Registro adicionado com sucesso.',
		'RS02' => 'Registro removido com sucesso.',

		'RF01' => 'Registro não encontrado.',
		'RF02' => 'Registro não adicionado.',

		'PI01' => 'Parâmetro Inválido.',

		'ER01' => 'Erro ao remover registro.',

        'SE01' => 'Sucesso.',
        'SE02' => 'Falha de autenticacao.',
        'SE03' => 'Token invalido.',
        'SE04' => 'Requisicao invalida.',
        'SE05' => 'Nao autorizado.',

        'EX01' => 'Rota nao encontrada.',
        'EX02' => 'Metodo invalido. Verifique o metodo utilizado (GET, POST, ...) e tente novamente.'
		);
	
	public static function getMSG($idMSG){
		return self::$aMSG[$idMSG];
	}
}

?>