<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Dieta extends Model
{
    protected $table = 'dieta';

	protected $fillable = ['caloria', 'refeicao'];

    public $timestamps = false;
}