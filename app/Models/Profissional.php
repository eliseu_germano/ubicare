<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Profissional extends Model implements Perfil
{
	protected $table = 'Profissional';

	protected $fillable = ['profissao', 'usuario_id'];

	public function allPerfis(){
		return self::all();
	}

	public function getPerfil($id){
		$perfil = self::find($id);

    	if(is_null($perfil)) return false;

    	return $perfil;
	}

	public function savePerfil($usuario_id){
		$input = Input::all();
		$input['usuario_id'] = $usuario_id;

    	$perfil = new Profissional();
    	$perfil->fill($input);
    	return $perfil->save();
	}

	public function updatePerfil($id){
		$perfil = self::find($id);

    	if(is_null($perfil)) return false;

    	$input = Input::all();
    	$perfil->fill($input);
    	$perfil->save();
    	return $perfil;
	}
	
	public function deletePerfil($id){
    	$perfil = self::find($id);

    	if(is_null($perfil)) return false;

    	return $perfil->delete();
    }

    public function getUsuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }
}
