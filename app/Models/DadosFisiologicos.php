<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DadosFisiologicos extends Model
{
    protected $table = 'dadosfisiologicos';

    protected $fillable = ['pressao', 'dataPressao', 'horaPressao', 'pulsacao', 'dataPulsacao', 'horaPulsacao', 'temperatura', 'dataTemperatura', 'horaTemperatura'];

    public $timestamps = false;

    public function allDadosfisiologicos(){
        $dados=self::all();
        return $dados;
    }

    public function getDadosFisiologicos($id){
        $dados = self::find($id);

        if(is_null($dados)){
            return false;
        }
        return $dados;
    }

    public function saveDadosFisiologicos(){
        $input = Input::all();

        $dados = new DadosFisiologicos();
        $dados->fill($input);

        return $dados->save();
    }

    public function updateDadosFisiologicos($id){
        $dados = self::find($id);

        if(is_null($dados)){
            return false;
        }

        $input = Input::all();
        $dados->fill($input);
        $dados->save();
        return $dados;
    }

    public function deleteDadosFisiologicos($id){
        $dados = self::find($id);

        if(is_null($dados)){
            return false;
        }

        return $dados->delete();
    }
}

