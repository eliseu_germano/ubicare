<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Hash;

class Prescricao extends Model
{
	protected $table = 'prescricao';

    protected $fillable = 	[
    						'nome', 'tipo', 'descricao', 'horarios', 'dataInicio', 
    						'dataTermino', 'responsavel', 'observacao'
    						];

    public function allPrescricoes(){
    	return self::all();
    }

    public function getPrescricao($id){
    	$prescricao = self::find($id);

    	if(is_null($prescricao)){
    		return false;
    	}
    	return $prescricao;
    }

    public function savePrescricao(){
    	$input = Input::all();

    	$prescricao = new Prescricao();
    	$prescricao->fill($input);
        
    	return $prescricao->save();
    }

    public function updatePrescricao($id){
    	$prescricao = self::find($id);

    	if(is_null($prescricao)){
    		return false;
    	}

    	$input = Input::all();
    	$prescricao->fill($input);
    	$prescricao->save();
    	return $prescricao;
    }

    public function deletePrescricao($id){
    	$prescricao = self::find($id);

    	if(is_null($prescricao)){
    		return false;
    	}

    	return $prescricao->delete();
    }

    public function alertas(){
        return $this->hasMany('App\Models\Alerta');
    }
}