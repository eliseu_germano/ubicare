<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Hash;

class DadosBalanca extends Model
{
    protected $table = 'medicaobalanca';

    protected $fillable = ['peso', 'data', 'hora'];

    public function allDadosBalanca(){
    	return self::all();
    }

    public function getDadosBalanca($id){
    	$dadosBalanca = self::find($id);

    	if(is_null($dadosBalanca)){
    		return false;
    	}
    	return $dadosBalanca;
    }

    public function saveDadosBalanca(){
    	$input = Input::all();

    	$dadosBalanca = new DadosBalanca();
    	$dadosBalanca->fill($input);

    	return $dadosBalanca->save();
    }

    public function updateDadosBalanca($id){
    	$dadosBalanca = self::find($id);

    	if(is_null($dadosBalanca)){
    		return false;
    	}

    	$input = Input::all();
    	$dadosBalanca->fill($input);
    	$dadosBalanca->save();
    	return $dadosBalanca;
    }

    public function deleteDadosBalanca($id){
    	$dadosBalanca = self::find($id);

    	if(is_null($dadosBalanca)){
    		return false;
    	}

    	return $dadosBalanca->delete();
    }
}
