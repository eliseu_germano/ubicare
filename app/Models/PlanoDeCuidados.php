<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Plano_de_Cuidados extends Model
{
    protected $table = 'plano_de_cuidados';

	protected $fillable = ['tipoTratamento'];

    public $timestamps = false;
}