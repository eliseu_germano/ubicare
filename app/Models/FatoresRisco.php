<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class FatoresRisco extends Model
{
    protected $table = 'fatoresrisco';

	protected $fillable = ['fumante', 'diabetesMellitus', 'daop', 'icc', 'dac', 'alt', 'insuficienciaRenal', 'retinopatia'];

    public $timestamps = false;

    public function allFatoresRisco(){
        return self::all();
    }

    public function getFatoresRisco($id){
        $dados = self::find($id);

        if(is_null($dados)){
            return false;
        }
        return $dados;
    }

    public function saveFatoresRisco(){
        $input = Input::all();

        $dados = new FatoresRisco();
        $dados->fill($input);

        return $dados->save();
    }

    public function updateFatoresRisco($id){
        $dados = self::find($id);

        if(is_null($dados)){
            return false;
        }

        $input = Input::all();
        $dados->fill($input);
        $dados->save();
        return $dados;
    }

    public function deleteFatoresRisco($id){
        $dados = self::find($id);

        if(is_null($dados)){
            return false;
        }

        return $dados->delete();
    }
}