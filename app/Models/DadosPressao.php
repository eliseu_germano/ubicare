<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Hash;

class DadosPressao extends Model
{
    protected $table = 'medicaopressao';

    protected $fillable = ['sistolica', 'diastolica', 'media', 'pulso', 'data', 'hora'];

    public function allDadosPressao(){
    	return self::all();
    }

    public function getDadosPressao($id){
    	$dadosPressao = self::find($id);

    	if(is_null($dadosPressao)){
    		return false;
    	}
    	return $dadosPressao;
    }

    public function saveDadosPressao(){
    	$input = Input::all();

    	$dadosPressao = new DadosPressao();
    	$dadosPressao->fill($input);

    	return $dadosPressao->save();
    }

    public function updateDadosPressao($id){
    	$dadosPressao = self::find($id);

    	if(is_null($dadosPressao)){
    		return false;
    	}

    	$input = Input::all();
    	$dadosPressao->fill($input);
    	$dadosPressao->save();
    	return $dadosPressao;
    }

    public function deleteDadosPressao($id){
    	$dadosPressao = self::find($id);

    	if(is_null($dadosPressao)){
    		return false;
    	}

    	return $dadosPressao->delete();
    }
}
