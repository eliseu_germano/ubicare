<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class ExamesMedicos extends Model
{
    protected $table = 'examesmedicos';

	protected $fillable = ['dataRealizacao', 'preparacao'];

    public $timestamps = false;
}