<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class APIToken extends Model
{
    protected $table = 'token';

	protected $fillable = ['token', 'chave_api', 'ip', 'usuario', 'ultimo_uso'];

    public $timestamps = false;

    public function usuario_obj() {
        return $this->belongsTo('App\Models\Usuario', 'usuario');
    }
}
