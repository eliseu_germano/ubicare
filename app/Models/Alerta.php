<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Alerta extends Model
{
    protected $table = 'alerta';
    protected $fillable = ['mensagem', 'horario', 'prescricao_id'];

    public function allAlertas(){
    	return self::all();
    }

    public function getAlerta($id){
    	$alerta = self::find($id);

    	if(is_null($alerta)){
    		return false;
    	}
    	return $alerta;
    }

    public function saveAlerta(){
    	$input = Input::all();

    	$alerta = new Alerta();
    	$alerta->fill($input);

    	return $alerta->save();
    }

    public function updateAlerta($id){
    	$alerta = self::find($id);

    	if(is_null($alerta)){
    		return false;
    	}

    	$input = Input::all();
    	$alerta->fill($input);
    	$alerta->save();
    	return $alerta;
    }

    public function deleteAlerta($id){
    	$alerta = self::find($id);

    	if(is_null($alerta)){
    		return false;
    	}

    	return $alerta->delete();
    }

    public function getPrescricao(){
    	return $this->belongsTo('App\Models\Prescricao');
    }
}
