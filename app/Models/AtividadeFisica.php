<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    protected $table = 'atividadefisica';

    protected $fillable = ['periodo', 'duracao'];

    public $timestamps = false;
}
