<?php

namespace App\Models;

use App\Perfil;

class PerfilFactory
{
	public static function factoryMethod($perfil){
		switch ($perfil) {
			case 'paciente':
				return new Paciente();
			
			case 'profissional':
				return new Profissional();

			case 'familia':
				return new Familia();

			default:
				return NULL;
		}
	}
}

?>