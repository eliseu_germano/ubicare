<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Hash;

class Usuario extends Model
{
	protected $table = 'Usuario';

	protected $fillable = ['nome', 'sexo', 'login', 'email', 'senha', 'perfil'];

	public function allUsuarios(){
	    return self::all();
	}

	public function getUsuario($id){
		$usuario = self::find($id);

    	if(is_null($usuario)) return false;

    	return $usuario;
	}

	public function saveUsuario(){
		$input = Input::all();
		$input['senha'] = Hash::make($input['senha']);
		$usuario = new Usuario();
		$usuario->fill($input);
		$usuario->save();
		return $usuario->id;
	}

	public function updateUsuario($id){
    	$usuario = self::find($id);

    	if(is_null($usuario)){
    		return false;
    	}

    	$input = Input::all();
    	$usuario->fill($input);
    	$usuario->save();
    	return $usuario;
    }

    public function deleteUsuario($id){
    	$usuario = self::find($id);

    	if(is_null($usuario)){
    		return false;
    	}

    	return $usuario->delete();
    }

	/* 
	
	relationships [Usuario - Perfil]

	*/

	public function profissional(){
        return $this->hasOne('App\Models\Profissional');
    }
    
    public function paciente(){
        return $this->hasOne('App\Models\Paciente');
    }
    
    public function familia(){
        return $this->hasOne('App\Models\Familia');
    }
}
