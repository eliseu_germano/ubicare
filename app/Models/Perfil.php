<?php

namespace App\Models;

interface Perfil
{
	/* methods use to implements the CRUD of Perfil */
	
	function allPerfis();
	function getPerfil($id);
	function savePerfil($usuario_id);
	function updatePerfil($id);
	function deletePerfil($id);

	/* use to implements the relashionship one to many between Usuario and Perfil */
	
	function getUsuario();
}

?>