<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class NotificacoesSemana extends Model
{
    protected $table = 'notificacoes_semana';

	protected $fillable = ['nome', 'data', 'hora'];

    public $timestamps = false;
}