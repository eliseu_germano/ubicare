<?php

namespace App\Models;

use App\Models\Perfil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Medicamentos extends Model
{
    protected $table = 'medicamentos';

	protected $fillable = ['instrucaoUso'];

    public $timestamps = false;
}