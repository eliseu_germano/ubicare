<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('Usuario')->insertGetId([
            'nome' => 'Natalia',
            'sexo' => 'feminino',
            'login' => 'nat',
            'email' => 'nat@email.com',
            'senha' => Hash::make('123'),
            'perfil' => 'profissional'
        ]);

        DB::table('Profissional')->insert([
            'profissao' => 'analista',
            'usuario_id' => $id
        ]);
    }
}
