<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerta', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->time('horario');
            $table->text('mensagem');
            $table->integer('prescricao_id')->unsigned();
            $table->foreign('prescricao_id')->references('id')->on('prescricao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alerta');
    }
}
