<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Usuario', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id', 50);
            $table->string('nome', 100);
            $table->string('sexo', 20);
            $table->string('login', 50);
            $table->string('email', 50);
            $table->string('senha', 60);
            $table->string('perfil', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Usuario');
    }
}
