<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFatoresDeRisco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fatoresrisco', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->tinyInteger('fumante');
            $table->tinyInteger('diabetesMellitus');
            $table->tinyInteger('daop');
            $table->tinyInteger('icc');
            $table->tinyInteger('dac');
            $table->tinyInteger('alt');
            $table->tinyInteger('insuficienciaRenal');
            $table->tinyInteger('retinopatia');
            $table->integer('fk_id_plano_de_cuidados')->unsigned();
            $table->foreign('fk_id_plano_de_cuidados')->references('id')->on('plano_de_cuidados');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dieta');
    }
}
