<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('token');
            $table->string('chave_api', 256);
            $table->string('ip');
            $table->integer('usuario')->unsigned();
            $table->foreign('usuario')->references('id')->on('Usuario');
            $table->datetime('ultimo_uso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('token');
    }
}
