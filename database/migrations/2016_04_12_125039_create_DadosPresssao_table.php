<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDadosPresssaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicaopressao', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->float('sistolica');
            $table->float('diastolica');
            $table->float('media');
            $table->float('pulso');
            $table->date('data');
            $table->time('hora');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicaopressao');
    }
}
