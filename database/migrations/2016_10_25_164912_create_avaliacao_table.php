<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacao', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->dateTime('data');
            $table->text('descricao');
            $table->string('resultado', 45);
            $table->dateTime('proxima_avaliacao');
            $table->integer('id_profissional')->unsigned();
            $table->foreign('id_profissional')->references('id')->on('Profissional');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avaliacao');
    }
}
