<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Paciente', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('dataNascimento');
            $table->string('responsavel', 100);
            $table->float('altura');
            $table->float('peso');
            $table->float('taxaGordura');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('Usuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Paciente');
    }
}
