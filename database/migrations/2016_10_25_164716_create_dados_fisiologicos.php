<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDadosFisiologicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dadosfisiologicos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pressao');
            $table->date('dataPressao');
            $table->time('horaPressao');
            $table->integer('pulsacao');
            $table->date('dataPulsacao');
            $table->time('horaPulsacao');
            $table->double('temperatura');
            $table->date('dataTemperatura');
            $table->time('horaTemperatura');
            $table->integer('fk_id_paciente')->unsigned();
            $table->foreign('fk_id_paciente')->references('id')->on('Paciente');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atividade_fisicas');
    }
}
