<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfissionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Profissional', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('profissao', 50);
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('Usuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Profissional');
    }
}
