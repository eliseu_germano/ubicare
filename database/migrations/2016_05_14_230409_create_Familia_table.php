<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Familia', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('parentesco', 50);
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('Usuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Familia');
    }
}
